---
title: "Links"
date: "2025-01-25"
author: "H."
---

## eZines

- [PoC||GTFO](https://www.alchemistowl.org/pocorgtfo/)
- [tmp.out](https://tmpout.sh/)
- [AppSec Ezine](https://github.com/Simpsonpt/AppSecEzine)
- [Paged Out!](https://pagedout.institute/)

## Challenges

- [Binary Golf](https://binary.golf/)

## People

- [Fabien Sanglard](https://fabiensanglard.net/): many retro stuff, mostly retro games analysis.
